package com.sensedia.customer.adapters.amqp;

import com.sensedia.commons.errors.domains.DefaultErrorResponse;
import com.sensedia.customer.adapters.amqp.config.BrokerOutput;
import com.sensedia.customer.adapters.mappers.CustomerMapper;
import com.sensedia.customer.domains.Customer;
import com.sensedia.customer.ports.AmqpPort;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

import static com.sensedia.commons.headers.DefaultHeader.APP_ID_HEADER_NAME;
import static com.sensedia.commons.headers.DefaultHeader.EVENT_NAME_HEADER_HEADER;
import static com.sensedia.customer.adapters.amqp.config.EventConfig.*;

@Service
@EnableBinding({BrokerOutput.class})
public class AmqpCustomerAdapterOutbound implements AmqpPort {

  private final BrokerOutput output;
  private final CustomerMapper customerMapper;

  @Value("${spring.application.name}")
  protected String appId;

  @Autowired
  public AmqpCustomerAdapterOutbound(BrokerOutput output, CustomerMapper customerMapper) {
    this.output = output;
    this.customerMapper = customerMapper;
  }

  @Override
  public void notifyCustomerCreation(Customer customer) {
    sendMessage(output.publishCustomerCreated(), customer, CUSTOMER_CREATION_EVENT_NAME);
  }

  @Override
  public void notifyCustomerOperationError(DefaultErrorResponse errorResponse) {
    sendMessage(output.publishCustomerOperationError(), errorResponse, CUSTOMER_OPERATION_ERROR_EVENT_NAME);
  }

  private void sendMessage(MessageChannel channel, Customer customer, String eventName) {
    sendMessage(channel, customerMapper.toCustomerDto(customer), eventName);
  }

  private void sendMessage(MessageChannel channel, Object object, String eventName) {
    channel.send(
        MessageBuilder.withPayload(object)
            .setHeader(EVENT_NAME_HEADER_HEADER, eventName)
            .setHeader(APP_ID_HEADER_NAME, appId)
            .build());
  }
}
