package com.sensedia.customer.adapters.http;

import com.sensedia.customer.adapters.dtos.QualificationsDtoResponse;
import com.sensedia.customer.domains.Customer;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;


@FeignClient(name = "${qualifications.name}", url = "${qualifications.url}/${qualifications.version}")
public interface QualificationsClient {

    @PostMapping("/qualifications")
    ResponseEntity<QualificationsDtoResponse> qualify(@Valid @RequestBody Customer customer);

}
