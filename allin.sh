#/bin/bash

export BASE_NAME="$1"

docker run -it --rm  -v "$(pwd)":/usr/src/mymaven -w /usr/src/mymaven maven:3.8.2-openjdk-11 mvn clean package --file apix-microservices-account/pom.xml

docker run -it --rm  -v "$(pwd)":/usr/src/mymaven -w /usr/src/mymaven maven:3.8.2-openjdk-11 mvn clean package --file apix-microservices-customer/pom.xml

docker run -it --rm  -v "$(pwd)":/usr/src/mymaven -w /usr/src/mymaven maven:3.8.2-openjdk-11 mvn clean package --file apix-microservices-notification/pom.xml


aws ecr get-login-password --region us-east-2 | docker login --username AWS --password-stdin 613739954613.dkr.ecr.us-east-2.amazonaws.com

cd apix-microservices-account
docker build -t apix2021/microservices-account-$BASE_NAME .
docker tag apix2021/microservices-account-$BASE_NAME:latest 613739954613.dkr.ecr.us-east-2.amazonaws.com/apix2021/microservices-account:$BASE_NAME
docker push 613739954613.dkr.ecr.us-east-2.amazonaws.com/apix2021/microservices-account:$BASE_NAME
docker rmi 613739954613.dkr.ecr.us-east-2.amazonaws.com/apix2021/microservices-account:$BASE_NAME
docker rmi apix2021/microservices-account-$BASE_NAME
cd ..

cd apix-microservices-notification
docker build -t apix2021/microservices-notification-$BASE_NAME .
docker tag apix2021/microservices-notification-$BASE_NAME:latest 613739954613.dkr.ecr.us-east-2.amazonaws.com/apix2021/microservices-notification:$BASE_NAME
docker push 613739954613.dkr.ecr.us-east-2.amazonaws.com/apix2021/microservices-notification:$BASE_NAME
docker rmi 613739954613.dkr.ecr.us-east-2.amazonaws.com/apix2021/microservices-notification:$BASE_NAME
docker rmi apix2021/microservices-notification-$BASE_NAME
cd ..

cd apix-microservices-customer
docker build -t apix2021/microservices-customer-$BASE_NAME .
docker tag apix2021/microservices-customer-$BASE_NAME:latest 613739954613.dkr.ecr.us-east-2.amazonaws.com/apix2021/microservices-customer:$BASE_NAME
docker push 613739954613.dkr.ecr.us-east-2.amazonaws.com/apix2021/microservices-customer:$BASE_NAME
docker rmi 613739954613.dkr.ecr.us-east-2.amazonaws.com/apix2021/microservices-customer:$BASE_NAME
docker rmi apix2021/microservices-customer-$BASE_NAME
cd ..

sed k8s/bundle/values-dev.yaml -e "s/latest/$BASE_NAME/g" > k8s/bundle/values-pipeline.yaml

helm upgrade -i apix-$BASE_NAME k8s/bundle/ --values k8s/bundle/values-pipeline.yaml  -n microservices-sandbox
