package ${package}.notification.adapters.http;

import ${package}.notification.adapters.dtos.NotificationDto;
import ${package}.notification.domains.Notification;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.constraints.NotEmpty;


@FeignClient(name = "${notification.name}", url = "${notification.url}/${notification.version}")
public interface SlackClient {

    @RequestMapping(method = RequestMethod.POST, value = "/notifications")
    void publishMessage(NotificationDto notification);

}
